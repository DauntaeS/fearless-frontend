function createCard(
  title,
  description,
  pictureUrl,
  location,
  startDate,
  endDate
) {
  return `
      <div class="card shadow p-3 mb-5 bg-body-tertiary rounded ">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class ="card-footer">
        ${new Date(startDate).toLocaleDateString()}
        -
        ${new Date(endDate).toLocaleDateString()}
      </div>
    `;
}

window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
    } else {
      const data = await response.json();

      let index = 0;
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const location = details.conference.location.name;
          console.log(location);
          const startDate = details.conference.starts;
          const endDate = details.conference.ends;
          const pictureUrl = details.conference.location.picture_url;
          const html = createCard(
            title,
            description,
            pictureUrl,
            location,
            startDate,
            endDate
          );
          const column = document.getElementById(`col-${index % 3}`);
          column.innerHTML += html;
          index += 1;
        }
      }
    }
  } catch (e) {
    // Figure out what to do if an error is raised
    console.error("error", e);
  }
});
