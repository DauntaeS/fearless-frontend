import React, { useEffect, useState } from "react";

function ConferenceForm(props) {
    const [name, setName] = useState('');
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');
    const [description, setDescription] = useState('');
    const [max_presentation, setPresentation] = useState('');
    const [max_attendees, setAttendeesCount] = useState('');
    const [location, setLocation] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
      }

      const handleStartDateChange = (event) => {
        const value = event.target.value;
        setStartDate(value);
      }

      const handleEndDateChange = (event) => {
        const value = event.target.value;
        setEndDate(value);
      }

      const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
      }

      const handlePresentationChange = (event) => {
        const value = event.target.value;
        setPresentation(value);
      }

      const handleAttendeesChange = (event) => {
        const value = event.target.value;
        setAttendeesCount(value);
      }

      const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
      }

      const handleSubmit = (event) => {
        event.preventDefault();

        const data = {}

        data.name = name;
        data.starts = startDate
        data.ends = endDate
        data.description = description
        data.max_attendees = max_attendees
        data.max_presentations = max_presentation
        data.location = location
        console.log(data)


        const conferenceUrl = "http://localhost:8000/api/conferences/";
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            "Content-Type": "application/json",
          },
        };
        const response = fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
          const newConference = response.json();
          console.log(newConference);

          setName('');
          setAttendeesCount('');
          setDescription('');
          setStartDate('');
          setEndDate('');
          setLocation('');
          setPresentation('');
        }
      }

      const [locations, setLocations] = useState([]);
      const fetchData = async () => {
        const url = "http://localhost:8000/api/locations/";

        const response = await fetch(url);

        if(response.ok){
            const data = await response.json();
            setLocations(data.locations)
        }
      };

      useEffect(() => {
        fetchData();
      }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input
                  onChange={handleNameChange}
                  placeholder="Name"
                  value={name}
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleStartDateChange}
                  placeholder="Start Date"
                  value={startDate}
                  required
                  type="date"
                  name="starts"
                  id="starts"
                  className="form-control"
                />
                <label htmlFor="starts">Start Date</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleEndDateChange}
                  placeholder="End date"
                  value={endDate}
                  required
                  type="date"
                  name="ends"
                  id="ends"
                  className="form-control"
                />
                <label htmlFor="ends">End Date</label>
              </div>
              <label htmlFor="description" className="form-label">Description</label>
              <div className="form-floating mb-3">
                <textarea
                  onChange={handleDescriptionChange}
                  value={description}
                  className="form-control"
                  id="description"
                  rows="3"
                ></textarea>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handlePresentationChange}
                  placeholder="Max Presentations"
                  value={max_presentation}
                  required
                  type="number"
                  name="max_presentations"
                  id="max_presentations"
                  className="form-control"
                />
                <label htmlFor="ends">Max Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleAttendeesChange}
                  placeholder="Max Attendees"
                  value={max_attendees}
                  required
                  type="number"
                  name="max_attendees"
                  id="max_attendees"
                  className="form-control"
                />
                <label htmlFor="max_attendees">Max Attendees</label>
              </div>
              <div className="mb-3">
                <select
                  onChange={handleLocationChange}
                  value={location}
                  required
                  id="location"
                  name="location"
                  className="form-select"
                >
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return(
                    <option key={location.id} value={location.id}>
                        {location.name}
                    </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm;
